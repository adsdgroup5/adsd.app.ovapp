package adsd.app.ovapp;

import adsd.app.ovapp.bus.BusStops;
import adsd.app.ovapp.trein.Stations;
import adsd.app.ovapp.trein.TreinDataModel;
import adsd.app.ovapp.plane.PlaneDataModel;
import adsd.app.ovapp.plane.Airport;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;

public class OvApp {

    private Profile profile;
    ArrayList<Profile>  profileList;

    public OvApp() {
        ArrayList<Profile>  profileList =TreinDataModel.getUsers();
        new Login(profileList);
    }

    public static Profile getLoggedInUser(int userId, ArrayList<Profile> profileList) {
        Profile userLoggedInProfile = profileList.stream().filter(p->userId==p.getId()).findAny().orElse(null);
        return userLoggedInProfile;
    }
}


class DefaultFrame {

    JTabbedPane tabs;

    DefaultFrame(ArrayList<Profile> profileList,String lang,String country, int userId, boolean isAdmin) {

        Locale.setDefault(new Locale(lang, country));
        ResourceBundle chosenLanguage = ResourceBundle.getBundle("bundle");
        JFrame defaultFrame = new JFrame("OvApp");

        //set tabs for each type of transport
        tabs = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);

        ReisplannerTrijn reisplanner = new ReisplannerTrijn("trein",lang,country, userId, profileList);
        Gebruikersinformatie gebruikersinformatie = new Gebruikersinformatie(profileList,lang,country, isAdmin, userId);

        Favorites mijnTrajecten = new Favorites(lang, country);
        ImageIcon iconTrein = new ImageIcon(Functions.getUrl("trein.png"));
        ImageIcon iconBus = new ImageIcon( Functions.getUrl("bus.png"));
        ImageIcon iconUser = new ImageIcon(Functions.getUrl("user.png"));
        ImageIcon directions = new ImageIcon(Functions.getUrl("directions.png"));

        tabs.addTab(chosenLanguage.getString("reisplanner"), iconTrein, reisplanner,"trein");
        BusReisInfo busReisInfo = new BusReisInfo("bus",lang,country,userId,profileList);

        tabs.addTab(chosenLanguage.getString("reisplanner_bus"), iconBus,busReisInfo, "bus");
        AirplanePlanner vliegtuig = new AirplanePlanner(new Locale(lang, country));

        tabs.addTab(chosenLanguage.getString("vliegtuig_planner"), iconTrein, vliegtuig);
        tabs.addTab(chosenLanguage.getString("gebruikersinformatie"), iconUser, gebruikersinformatie);
        tabs.addTab(chosenLanguage.getString("mijn_trajecten"), directions, mijnTrajecten);

        defaultFrame.getContentPane().add(tabs);
        defaultFrame.setSize(800, 800);
        defaultFrame.setVisible(true);
        defaultFrame.setLocationRelativeTo(null);

        JPanel BottomPanel=new JPanel();
        JButton uitloggen = new JButton(chosenLanguage.getString("uitloggen"));

        BottomPanel.add(uitloggen);
        uitloggen.setIcon(new ImageIcon(Functions.getUrl("logout.png")));

        Image icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Functions.getUrl("icon2.png"))).getImage();
        defaultFrame.setIconImage(icon);
        defaultFrame.add(BottomPanel,BorderLayout.SOUTH);
        defaultFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        uitloggen.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                defaultFrame.dispose();
                Login login=new Login(profileList);
            }
        });
    }
}

// Creating ReisplannerTrijn by extending JPanel
class ReisplannerTrijn  extends JPanel {

    String vervoerstype = "trein";
    private static boolean fromCurrentLocation;

    ReisplannerTrijn (String vervoerstype, String lang, String country,int userId, ArrayList<Profile> profileList ) {

        //get all the user information from Arraylist with saved profiles
        Profile user_logged_in_profile=OvApp.getLoggedInUser(userId, profileList);
        String location=user_logged_in_profile.getStraatNaam()+" ,"+user_logged_in_profile.getWoonplaats();
        String userName=user_logged_in_profile.getVoorNaam();
        String userLocation=user_logged_in_profile.getCloseToStation();

        //layout
        setLayout(new BorderLayout());
        Locale.setDefault(new Locale(lang, country));
        ResourceBundle chosenLanguage = ResourceBundle.getBundle("bundle");

        JPanel tablePanel=new JPanel();
        add(tablePanel,BorderLayout.CENTER);
        JPanel leftPanel=new JPanel();
        add(leftPanel, BorderLayout.WEST);

        Border line=BorderFactory.createLineBorder(Color.DARK_GRAY,1);
        JLabel welcome=new JLabel(chosenLanguage.getString("welkom")+", "+userName);
        welcome.setFont(new Font("verdana", Font.PLAIN, 14));
        leftPanel.setLayout(null);

        welcome.setBounds(25,50,200,18);
        JLabel locatie=new JLabel(chosenLanguage.getString("locatie"));
        JLabel locationLabel=new JLabel(location);

        Functions.setLink(locationLabel);
        locationLabel.setToolTipText("plan vanaf je huidige locatie");
        locatie.setBounds(25,75,300,14);
        locatie.setFont(new Font("verdana", Font.PLAIN, 12));

        tablePanel.add(welcome);
        tablePanel.add(locatie);
        tablePanel.add(locationLabel);

        JLabel aankomstenLabel=new JLabel(chosenLanguage.getString("aankomsten")+">>>");
        Functions.setLink(aankomstenLabel);
        JLabel vertragingsLabel=new JLabel(chosenLanguage.getString("Alle_informatie_over_vertragingen")+">>>");
        Functions.setLink(vertragingsLabel);

        // call a Stations class constructor, if the currunt location defined  by user trough JText input, FromCurrentLocation=false
        tablePanel.add(new Stations(false,lang,country));

        vertragingsLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                JFrame vertragingsFrame=new JFrame();
                vertragingsFrame.setSize(800,700);
                DefaultMutableTreeNode node=new DefaultMutableTreeNode("vertragingen:");
                ArrayList<ArrayList<String>> arrayOfDelays=TreinDataModel.getallDelaysList();
                for(int i=0;i<arrayOfDelays.size();i++) {
                    node.add(new DefaultMutableTreeNode("van " + arrayOfDelays.get(i).get(0)+" naar "+arrayOfDelays.get(i).get(arrayOfDelays.get(i).size()-1) ));
                }

                JPanel treePanel=new JPanel();
                treePanel.setLayout (new FlowLayout(FlowLayout.CENTER));

                JTree delaysTree=new JTree(node);
                Icon icon_i = new ImageIcon(Functions.getUrl("icon_i.png"));
                DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();

                renderer.setOpenIcon(icon_i);
                renderer.setClosedIcon(icon_i);
                renderer.setLeafIcon(icon_i);

                delaysTree.setCellRenderer(renderer);
                vertragingsFrame.add(delaysTree);
                vertragingsFrame.setVisible(true);
                vertragingsFrame.setLocationRelativeTo(null);

                Image icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Functions.getUrl("icon_i.png"))).getImage();
                vertragingsFrame.setIconImage(icon);
            }
        });

        locationLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                JFrame fromCurrentLocationframe=new JFrame();

                //call a Stations class constructor with true value of vaiable fromCurentLocation
                fromCurrentLocationframe.add(new Stations(userLocation,userName, true,lang,country));
                fromCurrentLocationframe.setSize(800,700);
                fromCurrentLocationframe.setVisible(true);
                fromCurrentLocationframe.setLocationRelativeTo(null);

            }
        });

        JPanel bottomPanel=new JPanel(); tablePanel.add(bottomPanel);
        bottomPanel.setLayout (new BoxLayout (bottomPanel, BoxLayout.Y_AXIS));
        bottomPanel.add(aankomstenLabel);
        aankomstenLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        bottomPanel.add(vertragingsLabel);

        tablePanel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                JFrame frame=new JFrame();
                frame.add(new Stations(lang,country));
                frame.setSize(800,700);
                frame.setVisible(true);
                frame.setLocationRelativeTo(null);
                Image icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Functions.getUrl("icon2.png"))).getImage();
                frame.setIconImage(icon);
            }
        });
    }
}

class BusReisInfo extends JPanel {

    String vervoerstype = "bus";
    private static boolean fromCurrentLocation;
    private static int userIndex;

    BusReisInfo (String vervoerstype, String lang, String country,int userId, ArrayList<Profile> profileList) {
        Profile user_logged_in_profile=OvApp.getLoggedInUser(userId, profileList);
        setLayout(new BorderLayout());

        String separator=File.separator;
        Locale.setDefault(new Locale(lang, country));
        ResourceBundle chosenLanguage = ResourceBundle.getBundle("bundle");

        JPanel tablePanel=new JPanel();
        add(tablePanel,BorderLayout.CENTER);
        JPanel leftPanel=new JPanel();
        add(leftPanel, BorderLayout.WEST);

        Border line=BorderFactory.createLineBorder(Color.DARK_GRAY,1);
        JLabel welcome=new JLabel(chosenLanguage.getString("welkom")+", "+user_logged_in_profile.getVoorNaam());
        welcome.setFont(new Font("verdana", Font.PLAIN, 14));

        leftPanel.setLayout(null);
        welcome.setBounds(25,50,200,18);

        SimpleDateFormat formatter=new SimpleDateFormat("HH:mm, dd/MM/yyyy");
        Date date =new Date(System.currentTimeMillis());
        JLabel currentTime=new JLabel(formatter.format(date));
        currentTime.setFont(new Font("verdana", Font.PLAIN, 14));
        currentTime.setBounds(25,25,200,18);

        String location=user_logged_in_profile.getStraatNaam()+" ,"+profileList.get(userIndex).getWoonplaats();
        String userName=user_logged_in_profile.getVoorNaam();
        String userResidence=user_logged_in_profile.getWoonplaats();
        String userNearestBusStaion=user_logged_in_profile.getCloseToBusStop();

        JLabel locatie=new JLabel(chosenLanguage.getString("locatie"));
        JLabel locationLabel=new JLabel(location);
        Functions.setLink(locationLabel);

        locationLabel.setToolTipText("plan vanaf je huidige locatie");
        locatie.setBounds(25,75,300,14);
        locatie.setFont(new Font("verdana", Font.PLAIN, 12));

        tablePanel.add(welcome);
        tablePanel.add(locatie);
        tablePanel.add(locationLabel);

        String userLocation=user_logged_in_profile.getCloseToStation();
        tablePanel.add(new BusStops(userLocation,userName, false, userResidence,userNearestBusStaion,lang,country));

        locationLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                JFrame frame=new JFrame();
                frame.add(new BusStops(userLocation,userName, true, userResidence,userNearestBusStaion,lang,country));
                frame.setSize(800,700);
                frame.setVisible(true);
                frame.setLocationRelativeTo(null);
                Image icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Functions.getUrl("icon2.png"))).getImage();
                frame.setIconImage(icon);
            }
        });
    }
}

class Gebruikersinformatie  extends JPanel {

    Gebruikersinformatie(ArrayList<Profile> profileList, String lang, String country, boolean isAdmin, int userId) {
        Profile loggedInUser = OvApp.getLoggedInUser(userId, profileList);
        Locale.setDefault(new Locale(lang, country));
        ResourceBundle chosenLanguage = ResourceBundle.getBundle("bundle");
        setLayout(new BorderLayout());

        // administrator  is identified with ID  1
        // administrator has access to each of the saved profiles,  usual user has access only to his profile
        Object[][] data = (userId == 1) ? new Object[profileList.size()][] : new Object[1][];

        if (userId == 1) {
            for (int i = 0; i < profileList.size(); i++) {
                Profile profile = profileList.get(i);
                data[i] = new Object[]{profile.getId(), profile.getVoorNaam(), profile.getAchterNaam(), String.valueOf(profile.getLeeftijd()), profile.getWoonplaats(), profile.getStraatNaam()};
            }

        } else {
            data[0] = new Object[]{loggedInUser.getId(), loggedInUser.getVoorNaam(), loggedInUser.getAchterNaam(), String.valueOf(loggedInUser.getLeeftijd()), loggedInUser.getWoonplaats(), loggedInUser.getStraatNaam()};
        }

        // create table with all profiles of  users or only with your profile

        String column[] = {chosenLanguage.getString("id"), chosenLanguage.getString("voornaam"), chosenLanguage.getString("achternaam"), chosenLanguage.getString("leeftijd"), chosenLanguage.getString("woonplaats"), chosenLanguage.getString("straat")};

        JTable profielTable = new JTable(data, column);
        profielTable.setModel(new DefaultTableModel(data, column));
        JScrollPane scroolPane = new JScrollPane(profielTable);
        add(scroolPane, BorderLayout.WEST);

        JPanel innerPanel = new JPanel();
        add(innerPanel, BorderLayout.CENTER);
        innerPanel.setLayout(new GridLayout(20, 2));

        JLabel id = new JLabel(chosenLanguage.getString("id"));
        innerPanel.add(id);
        JTextField fieldId = new JTextField(2);
        innerPanel.add(fieldId);

        JLabel voornaam = new JLabel(chosenLanguage.getString("voornaam"));
        innerPanel.add(voornaam);
        JTextField fieldVoornaam = new JTextField(12);
        innerPanel.add(fieldVoornaam);

        JLabel achternaam = new JLabel(chosenLanguage.getString("achternaam"));
        innerPanel.add(achternaam);
        JTextField fieldAchternaam = new JTextField(12);
        innerPanel.add(fieldAchternaam);

        JLabel leeftijd = new JLabel(chosenLanguage.getString("leeftijd"));
        innerPanel.add(leeftijd);
        JTextField fieldLeeftijd = new JTextField(2);
        innerPanel.add(fieldLeeftijd);

        JLabel woonplaats = new JLabel(chosenLanguage.getString("woonplaats"));
        innerPanel.add(woonplaats);
        JTextField fieldWoonplats = new JTextField(15);
        innerPanel.add(fieldWoonplats);

        JLabel straat = new JLabel(chosenLanguage.getString("straat"));
        innerPanel.add(straat);
        JTextField fieldStraat = new JTextField(15);
        innerPanel.add(fieldStraat);

        profielTable.setAutoResizeMode(4);
        JButton opslaan = new JButton(chosenLanguage.getString("opslaan"));
        innerPanel.add(opslaan);

        profielTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DefaultTableModel model = (DefaultTableModel) profielTable.getModel();
                int selectedRowIndex = profielTable.getSelectedRow();
                int selectedColIndex = profielTable.getSelectedColumn();
                String cellValue = String.valueOf(profielTable.getValueAt(selectedRowIndex, selectedColIndex));
                fieldId.setText(model.getValueAt(selectedRowIndex, 0).toString());
                fieldVoornaam.setText(model.getValueAt(selectedRowIndex, 1).toString());
                fieldAchternaam.setText(model.getValueAt(selectedRowIndex, 2).toString());
                fieldLeeftijd.setText(model.getValueAt(selectedRowIndex, 3).toString());
                fieldWoonplats.setText(model.getValueAt(selectedRowIndex, 4).toString());
                fieldStraat.setText(model.getValueAt(selectedRowIndex, 5).toString());
            }
        });

        opslaan.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int i = profielTable.getSelectedRow();
                DefaultTableModel model = (DefaultTableModel) profielTable.getModel();
                if (i >= 0) {
                    model.setValueAt(fieldId.getText(), i, 0);
                    model.setValueAt(fieldVoornaam.getText(), i, 1);
                    model.setValueAt(fieldAchternaam.getText(), i, 2);
                    model.setValueAt(fieldLeeftijd.getText(), i, 3);
                    model.setValueAt(fieldWoonplats.getText(), i, 4);
                    model.setValueAt(fieldStraat.getText(), i, 5);
                    profileList.get(i).setId(Integer.parseInt(fieldId.getText()));
                    profileList.get(i).setVoorNaam(fieldVoornaam.getText());
                    profileList.get(i).setAchterNaam(fieldAchternaam.getText());
                    profileList.get(i).setLeeftijd(Integer.parseInt(fieldLeeftijd.getText()));
                    profileList.get(i).setWoonplaats(fieldWoonplats.getText());
                    profileList.get(i).setStraatNaam(fieldStraat.getText());

                } else {
                    JOptionPane.showMessageDialog(null, "Error");
                }
            }
        });
    }
}

class AirplanePlanner extends JPanel {

        PlaneDataModel planeDataModel;
        JTable table;

        public AirplanePlanner(Locale locale) {

            int widthPane = 800; //pixels
            int heightPane = 800; // pixels
            int offSetAlignize = 10; //pixels

            planeDataModel = new PlaneDataModel(locale);

            JTabbedPane tabbedPane = new JTabbedPane();
            tabbedPane.setBounds(offSetAlignize,
                    offSetAlignize,
                    widthPane - (40 * offSetAlignize),   //780
                    heightPane - (40 * offSetAlignize)); //760
            
            tabbedPane.add("Vliegtickets", getPanelTickets());

            add(tabbedPane);

            setLayout(new BorderLayout());
            setSize(1000,900);
            setVisible(true);
        }

        public JPanel getPanelTickets() {
            JPanel panel = new JPanel();
            panel.setLayout(null);
            panel.setBackground(Color.white);

            JLabel searchTickets = new JLabel("Zoek Vliegtickets");
            searchTickets.setFont(new Font("sa", Font.BOLD, 17));
            searchTickets.setBounds(10,8,250,50);

            JLabel departureLabel = new JLabel("Van");
            departureLabel.setBounds(10,90,100,20);

            JLabel arrivalLabel = new JLabel("Naar");
            arrivalLabel.setBounds(10,130,100,20);

            JLabel departureDateLabel = new JLabel("Vertrekdatum");
            departureDateLabel.setBounds(10,170, 100, 20);

            JLabel passengersLabel = new JLabel("Passagiers");
            passengersLabel.setBounds(10,210,100,20);

            JComboBox departureBox = new JComboBox(planeDataModel.getDepartureAirports().toArray());
            departureBox.setBounds(100,80,280,50);

            JComboBox arrivalBox = new JComboBox(planeDataModel.getArrivalAirports().toArray());
            arrivalBox.setBounds(100,128,280,30);

            JDateChooser dateChooser = new JDateChooser();
            dateChooser.setDateFormatString("dd-MM-yyyy");
            dateChooser.setBounds(100, 170, 280, 30);

            int min = 1;
            int max = 20;
            int step = 1;
            int beginValue = 1;

            SpinnerModel model = new SpinnerNumberModel(beginValue, min, max, step);
            JSpinner spinner = new JSpinner(model);
            JFormattedTextField textField = ((JSpinner.NumberEditor) spinner.getEditor()).getTextField();

            ((NumberFormatter) textField.getFormatter()).setAllowsInvalid(false);
            spinner.setBounds(100,205,280,30);

            JButton searchButton = new JButton("Bekijk Vluchten");
            searchButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Airport departureAirport = (Airport) departureBox.getModel().getSelectedItem();
                    Airport arrivalAirport = (Airport) arrivalBox.getModel().getSelectedItem();
                    Date departureDate = dateChooser.getDate();
                    int passengers = (int) spinner.getModel().getValue();
                    table(departureDate, departureAirport, arrivalAirport, passengers);
                }
            });

            searchButton.setBounds(180,300,200,40);

            panel.add(searchTickets);
            panel.add(departureLabel);
            panel.add(arrivalLabel);
            panel.add(departureDateLabel);
            panel.add(passengersLabel);
            panel.add(departureBox);
            panel.add(arrivalBox);
            panel.add(dateChooser);
            panel.add(spinner);
            panel.add(searchButton);

            return panel;
        }

        public void table(Date departureDate, Airport departureAirport, Airport arrivalAirport, int passengers){
            JFrame frame = new JFrame();
            String[] columnData = {"Vertraging in minuten", "Boarding-Gate", "Vertrekplaats", "Boarding-Tijd", "Vertrekdatum ",
                                   "Vertrektijd", "Aankomstijd", "Bestemmingplaats", "Aankomst-Gate", "Prijs", "Afstand"};

            String [][]rowData = planeDataModel.getPossibleFlights(departureAirport, arrivalAirport, departureDate, passengers);

            table = new JTable(rowData, columnData);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            table.getColumnModel().getColumn(0).setPreferredWidth(130);
            table.getColumnModel().getColumn(1).setPreferredWidth(180);
            table.getColumnModel().getColumn(2).setPreferredWidth(180);
            table.getColumnModel().getColumn(3).setPreferredWidth(140);
            table.getColumnModel().getColumn(4).setPreferredWidth(80);
            table.getColumnModel().getColumn(5).setPreferredWidth(100);
            table.getColumnModel().getColumn(6).setPreferredWidth(200);
            table.getColumnModel().getColumn(7).setPreferredWidth(200);
            table.getColumnModel().getColumn(8).setPreferredWidth(200);
            table.getColumnModel().getColumn(9).setPreferredWidth(130);

            JScrollPane scrollPane = new JScrollPane(table);
            scrollPane.setPreferredSize(new Dimension(700, 150));

            frame.add(scrollPane, BorderLayout.CENTER);

            frame.setSize(600,400);
            frame.setVisible(true);
        }
    }