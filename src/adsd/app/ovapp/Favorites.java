package adsd.app.ovapp;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class Favorites extends JPanel {
    private static int number;
    static DefaultTableModel model;
    private static String filename;
    private static ArrayList<String[]>favoritesList;
    private static String defaultImg="map0.png";

    public Favorites(String lang, String country) {
        Locale.setDefault(new Locale(lang, country));
        ResourceBundle chosenLanguage = ResourceBundle.getBundle("bundle");

        setLayout(new BorderLayout());
        JPanel topPanel=new JPanel();
        JPanel bottompanel=new JPanel();

        add( topPanel, BorderLayout.NORTH);
        add(bottompanel,BorderLayout.CENTER);

        JPanel leftPanel=new JPanel();
        JPanel rightPanel=new JPanel();

        topPanel.setLayout(new BorderLayout());
        topPanel.add(leftPanel,BorderLayout.WEST);
        topPanel.add(rightPanel,BorderLayout.CENTER);

        ArrayList<String[]> favoritesList=createFavoritesList();
        String[][] data=getData(favoritesList);
        String column[]={chosenLanguage.getString("van"),chosenLanguage.getString("naar"), chosenLanguage.getString("tijd"), chosenLanguage.getString("type")};

        JTable favoritesTable=new JTable();
        favoritesTable.setModel(new DefaultTableModel(data, column));
        DefaultTableModel model = (DefaultTableModel)favoritesTable.getModel();

        JScrollPane scrollPane=new JScrollPane(favoritesTable);
        scrollPane.setPreferredSize(new Dimension(475,275));
        rightPanel.add(scrollPane);
        bottompanel.add(createImagePanel(defaultImg));

        JLabel from=new JLabel(chosenLanguage.getString("van"));
        leftPanel.add(from);
        JTextField textFiledFrom=new JTextField();
        leftPanel.add(textFiledFrom);

        JLabel to=new JLabel(chosenLanguage.getString("naar"));
        leftPanel.add(to);
        JTextField textFiledTo=new JTextField();
        leftPanel.add(textFiledTo);

        JLabel timeLabel=new JLabel(chosenLanguage.getString("tijd"));
        JTextField timeTextField=new JTextField();
        leftPanel.add(timeLabel);
        leftPanel.add(timeTextField);

        JLabel type=new JLabel(chosenLanguage.getString("type"));
        leftPanel.add(type);
        JTextField textFiledType=new JTextField();
        leftPanel.add(textFiledType);

        JButton opslaanBtn=new JButton(chosenLanguage.getString("traject_bewerken"));
        leftPanel.add(opslaanBtn);

        JButton toevoegenBtn=new JButton(chosenLanguage.getString("voeg_een_traject_toe"));
        leftPanel.add(toevoegenBtn);

        toevoegenBtn.setPreferredSize(new Dimension(245,27));
        opslaanBtn.setPreferredSize(new Dimension(245,27));

        Color lRed =new Color(199, 35, 35);
        Color lGreen=new Color(75, 177, 79);

        opslaanBtn.setBackground(lRed);
        opslaanBtn.setForeground(Color.WHITE);

        toevoegenBtn.setBackground(lGreen);
        toevoegenBtn.setForeground(Color.WHITE);

        from.setPreferredSize(new Dimension(245,27));
        to.setPreferredSize(new Dimension(245,27));
        type.setPreferredSize(new Dimension(245,27));

        textFiledFrom.setPreferredSize(new Dimension(245,27));
        textFiledFrom.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        textFiledTo.setPreferredSize(new Dimension(245,27));
        textFiledTo.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        textFiledType.setPreferredSize(new Dimension(245,27));
        textFiledType.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        timeLabel.setPreferredSize(new Dimension(245,27));
        timeTextField.setPreferredSize(new Dimension(245,27));
        timeTextField.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        leftPanel.setPreferredSize(new Dimension(250,320));

        favoritesTable.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DefaultTableModel model = (DefaultTableModel) favoritesTable.getModel();
                int selectedRowIndex =  favoritesTable.getSelectedRow();
                int selectedColIndex = favoritesTable.getSelectedColumn();
                textFiledFrom.setText(model.getValueAt(selectedRowIndex, 0).toString());
                textFiledTo.setText(model.getValueAt(selectedRowIndex, 1).toString());
                timeTextField.setText(model.getValueAt(selectedRowIndex, 2).toString());
                textFiledType.setText(model.getValueAt(selectedRowIndex, 3).toString());
                number=selectedRowIndex;

                if (number==0) {
                    bottompanel.removeAll();
                    filename=getImg(favoritesList,0);
                    bottompanel.add(createImagePanel(filename));
                    bottompanel.validate();
                    bottompanel.repaint();
                }

                else if (number==1) {
                    bottompanel.removeAll();
                    filename=getImg(favoritesList,1);
                    bottompanel.add(createImagePanel(filename));
                    bottompanel.validate();
                    bottompanel.repaint();
                }

                else if (number==2) {
                    bottompanel.removeAll();
                    filename=getImg(favoritesList,2);
                    bottompanel.add(createImagePanel(filename));
                    bottompanel.validate();
                    bottompanel.repaint();
                }

                else if (number==3) {
                    bottompanel.removeAll();
                    filename=getImg(favoritesList,3);
                    bottompanel.add(createImagePanel(filename));
                    bottompanel.validate();
                    bottompanel.repaint();
                }

                else {
                    bottompanel.removeAll();
                    filename="map.jpeg";
                    bottompanel.add(createImagePanel(filename));
                    bottompanel.validate();
                    bottompanel.repaint();
                }
            }
        });

        opslaanBtn.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                int selectedRow = favoritesTable.getSelectedRow();

                if(selectedRow >= 0) {
                    model.setValueAt(textFiledFrom.getText(), selectedRow, 0);
                    model.setValueAt(textFiledTo.getText(), selectedRow, 1);
                    model.setValueAt(timeTextField.getText(), selectedRow, 2);
                    model.setValueAt(textFiledType.getText(), selectedRow, 3);
                    changeData(favoritesList,textFiledFrom.getText(),textFiledTo.getText(),timeTextField.getText(),textFiledType.getText(),favoritesList.get(selectedRow)[4],selectedRow);
                }

                else {
                    JOptionPane.showMessageDialog(null, "Error");
                    }
            }
        });

        toevoegenBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String valuefrom=textFiledFrom.getText();
                String valueTo=textFiledTo.getText();
                String valueTime=timeTextField.getText();
                String valueType=textFiledType.getText();
                model.addRow(new Object[]{valuefrom,valueTo, valueTime, valueType});
                setData(favoritesList,valuefrom,valueTo,valueTime, valueType,"map.jpeg");
            }
        });
    }

    public JPanel createImagePanel(String filename) {
        JPanel imgPanel=new JPanel();
        BufferedImage image = null;

        try {
            image = ImageIO.read(Functions.getUrl(filename));
        }

        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        ImageIcon imageIcon = new ImageIcon(image);
        JLabel jLabel = new JLabel();
        jLabel.setIcon(imageIcon);
        imgPanel.add(jLabel, BorderLayout.CENTER);
        return imgPanel;
    }

    public static String[][] getData(ArrayList<String[]> favoritesList) {
        String[][] dataFromArray=new String[favoritesList.size()][4];
        dataFromArray=favoritesList.toArray(dataFromArray);
        return dataFromArray;
    }

    public static ArrayList<String[]> createFavoritesList() {
        ArrayList<String[]> favoritesList=new ArrayList<>();
        favoritesList.add(new String[]{"Amersfoort Centraal","Utrecht Centraal","09:00","Bus","map0.png"});
        favoritesList.add(new String[]{"Amsterdam Centraal","Hilversum","09:30","Trein","map1.png"});
        favoritesList.add(new String[]{"Amersfoort Centraal","Amersfoort Eemplein","08:30","Bus","map2.png"});

        return favoritesList;
    }

    public static String getImg (ArrayList<String[]> favoritesList, int number) {
        String img=favoritesList.get(number)[4];
        return img;
    }

    public static ArrayList<String[]> setData (ArrayList<String[]> favoritesList, String startPunt, String endPunt, String tijd, String type, String img) {
        favoritesList.add(new String[] {startPunt,endPunt, tijd, type, img});
        return favoritesList;
    }


    public static ArrayList<String[]> changeData (ArrayList<String[]> favoritesList, String startPunt, String endPunt, String tijd, String type, String img, int index) {
        favoritesList.set(index, new String[]{startPunt, endPunt, tijd, type, img});
        return favoritesList;
    }
    
    public static void main(String[] args) {
        JFrame frame=new JFrame();
        frame.add(new Favorites("nl","NL"));
        frame.setVisible(true);
        frame.setSize(800,800);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
    }

}
