package adsd.app.ovapp;

public class Profile {
    private String voorNaam;
    private String achterNaam;
    private int leeftijd;
    private String straatNaam;
    private String woonplaats;
    private int id;
    private String closeToStation;
    private String closeToBusStop;




    public Profile()
    {
        this(null, null, 0, null, null,0,null,null);
    }
    //
    public Profile(String voorNaam,
                   String achterNaam)
    {
        this(voorNaam, achterNaam, 0, null, null,0,null,null);
    }

    public String getCloseToStation() {
        return closeToStation;
    }

    public void setCloseToStation(String closeToStation) {
        this.closeToStation = closeToStation;
    }

    public String getCloseToBusStop() {
        return closeToBusStop;
    }

    public void setCloseToBusStop(String closeToBusStop) {
        this.closeToBusStop = closeToBusStop;
    }

    //
    public Profile(String voorNaam,
                   String achterNaam,
                   int leeftijd,
                   String straatNaam,
                   String woonplaats,
                   int id,
                   String closeToStation,
                   String closeToBusStop)
    {
        this.voorNaam = voorNaam;
        this.achterNaam = achterNaam;
        this.leeftijd = leeftijd;
        this.straatNaam = straatNaam;
        this.woonplaats = woonplaats;
        this.id=id;
        this.closeToStation=closeToStation;
        this.closeToBusStop=closeToBusStop;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVoorNaam() {
        return voorNaam;
    }

    public void setVoorNaam(String voorNaam) {
        this.voorNaam = voorNaam;
    }

    public String getAchterNaam() {
        return achterNaam;
    }

    public void setAchterNaam(String achterNaam) {
        this.achterNaam = achterNaam;
    }

    public int getLeeftijd() {
        return leeftijd;
    }

    public void setLeeftijd(int leeftijd) {
        this.leeftijd = leeftijd;
    }

    public String getStraatNaam() {
        return straatNaam;
    }

    public void setStraatNaam(String straatNaam) {
        this.straatNaam = straatNaam;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }
}

