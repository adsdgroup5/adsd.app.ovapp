package adsd.app.ovapp.plane;

public class Airport {

    private String name;
    private String code;
    private String country;

    public Airport(String name, String code, String country){
        this.name = name;
        this.code = code;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFullName() {
        return name + " (" + code + "), " + country; // volledige naam -> Schiphol Amsterdam (AMS), Nederland
    }

    @Override
    public String toString() {
        return name + " (" + code + "), " + country; // volledige naam -> Schiphol Amsterdam (AMS), Nederland
    }


}
