package adsd.app.ovapp.plane;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.*;

public class PlaneDataModel {
    private ArrayList<Airport> departureAirports;
    private ArrayList<Airport> arrivalAirports;
    private ArrayList<Flight> flights;

    public PlaneDataModel(Locale chosenLanguage) {
        parseDataAndBuildList(chosenLanguage);
    }

    public void parseDataAndBuildList(Locale chosenLanguage) {
        // Lijst met alle vliegvelden, de naam van het land is dynamisch en automatisch in de gekozen taal
        departureAirports = new ArrayList<>();
        arrivalAirports = new ArrayList<>();

        Locale nederland = new Locale("nl", "NL");
        Locale zuidAfrika = new Locale("en", "ZA");

        departureAirports.add(new Airport("Schiphol", "AMS", nederland.getDisplayCountry(chosenLanguage)));
        departureAirports.add(new Airport("Eindhoven Airport", "EIN", nederland.getDisplayCountry(chosenLanguage)));
        departureAirports.add(new Airport("Station Enschede", "QYE", nederland.getDisplayCountry(chosenLanguage)));

        arrivalAirports.add(new Airport("Londen - City Airport", "LCY", Locale.UK.getDisplayCountry(chosenLanguage)));
        arrivalAirports.add(new Airport("New York - La Guirdia Airport", "LGA", Locale.US.getDisplayCountry(chosenLanguage)));
        arrivalAirports.add(new Airport("Toronto - Pearson International Airport", "YYZ", Locale.CANADA.getDisplayCountry(chosenLanguage)));
        arrivalAirports.add(new Airport("Narita International Airport", "NRT", Locale.JAPAN.getDisplayCountry(chosenLanguage)));
        arrivalAirports.add(new Airport("Cape Town International Airport", "CPT", zuidAfrika.getDisplayCountry(chosenLanguage)));

        // Lijst met alle vluchten
        flights = new ArrayList<>();
        for (int i = 0; i < arrivalAirports.size(); i++) {
            Random random = new Random();
            int upperbound = 200; //generate random values from 0-199
            int int_random = random.nextInt(upperbound); // random vertraging

            flights.add(new Flight(departureAirports.get(0), "12:00", "13:00", "D10",
                         arrivalAirports.get(i), "15:00", "A6", 30.95, 200 + 100 * i, int_random));

            flights.add(new Flight(departureAirports.get(1), "12:00", "13:00", "D10",
                         arrivalAirports.get(i), "15:00", "A6", 30.95, 200 + 100 * i, int_random));

            flights.add(new Flight(departureAirports.get(2), "12:00", "13:00", "D10",
                         arrivalAirports.get(i), "15:00", "A6", 30.95, 200 + 100 * i, int_random));
        }
    }

    public String[][] getPossibleFlights(Airport departureAirport, Airport arrivalAirport, Date departureDate, int passengers) {

        ArrayList<String[]> possibleFlights = new ArrayList<>();

        for(int flightIndex = 0; flightIndex < flights.size(); flightIndex++){

            DateFormat dateTimeFormatter = new SimpleDateFormat("dd-MM-yyyy");
            String vertrekDatumAsString = dateTimeFormatter.format(departureDate);

            Flight flight = flights.get(flightIndex);

            if(flight.getDepartureAirport() == departureAirport
                    && flight.getArrivalAirport() == arrivalAirport
            && flight.getDepartureDate().equals(vertrekDatumAsString)){

                String[] possibleFlight = {String.valueOf(flight.getDelay()), flight.getDepartureGate(), departureAirport.getFullName(), flight.getBoardingTime(),
                                      vertrekDatumAsString, flight.getDepartureTime(), flight.getArrivalTime(), arrivalAirport.getFullName(),
                                      flight.getArrivalGate(), String.valueOf(flight.getPrice() * passengers), String.valueOf(flight.getDistance())};
                possibleFlights.add(possibleFlight);
            }
        }
        return possibleFlights.toArray(new String[possibleFlights.size()][]);
    }

    public ArrayList<Airport> getArrivalAirports() {
        return arrivalAirports;
    }

    public ArrayList<Airport> getDepartureAirports() {
        return departureAirports;
    }

}

