package adsd.app.ovapp.plane;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Flight {

    private Airport departureAirport;
    private String boardingTime;
    private String departureTime;
    private String departureGate;

    private String departureDate;

    private Airport arrivalAirport;
    private String arrivalTime;
    private String arrivalGate;

    private double price;
    private double distance;
    private int delay;

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public Flight(Airport departureAirport, String boardingTime, String departureTime, String departureGate,
                  Airport arrivalAirport, String arrivalTime, String arrivalGate, double price, double distance, int delay){
        this.departureAirport = departureAirport;
        this.boardingTime = boardingTime;
        this.departureTime = departureTime;
        this.departureGate = departureGate;

        String dateToday = LocalDate.now().format(dateTimeFormatter); //12-01-2020 ipv 12 jan. 2020
        this.departureDate = dateToday;

        this.arrivalAirport = arrivalAirport;
        this.arrivalTime = arrivalTime;
        this.arrivalGate = arrivalGate;

        this.price = price;
        this.distance = distance;
        this.delay = delay;
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(Airport departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getBoardingTime() {
        return boardingTime;
    }

    public void setBoardingTime(String boardingTime) {
        this.boardingTime = boardingTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDepartureGate() {
        return departureGate;
    }

    public void setDepartureGate(String departureGate) {
        this.departureGate = departureGate;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(Airport arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalGate() {
        return arrivalGate;
    }

    public void setArrivalGate(String arrivalGate) {
        this.arrivalGate = arrivalGate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getDelay(){
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}
