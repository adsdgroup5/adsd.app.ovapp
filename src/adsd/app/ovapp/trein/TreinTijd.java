package adsd.app.ovapp.trein;

import java.util.ArrayList;

public class TreinTijd {

    String aankomstTijd=null;
    String vertekTijd=null;
    String perron;
    String stationNaam;
    String plaatsnaam;
    String traject;
    String tijd;
    private double afstand;
    private int preis;
    String richting;
    ArrayList<String> tussenHaltes;
    //constructor with overloading


    // Constructors with overloading

    public TreinTijd()
    {
        this(null,null,null,null,null,0.0, 0,null,null);
    }

    public TreinTijd(String perron, String stationNaam, String plaatsnaam, String traject, String tijd) {
        this(perron,stationNaam,plaatsnaam,traject,tijd,0.0,0,null,null);
    }

    public TreinTijd(String perron, String traject,  double afstand, ArrayList<String> tussenHaltes) {
     this (perron,null, null,traject,null,afstand,0,null, tussenHaltes);
    }


    public TreinTijd(String perron, String stationNaam, String plaatsnaam, String traject, String tijd, double afstand, int preis, String richting, ArrayList<String> tussenHaltes) {
        this.perron = perron;
        this.stationNaam = stationNaam;
        this.plaatsnaam = plaatsnaam;
        this.traject = traject;
        this.tijd = tijd;
        this.afstand = afstand;
        this.preis = preis;
        this.richting = richting;
        this.tussenHaltes = tussenHaltes;
    }





    public double getAfstand() {
        return afstand;
    }

    public void setAfstand(double afstand) {
        this.afstand = afstand;
    }

    public int getPreis() {
        return preis;
    }

    public void setPreis(int preis) {
        this.preis = preis;
    }

    public String getRichting() {
        return richting;
    }

    public void setRichting(String richting) {
        this.richting = richting;
    }

    public ArrayList<String> getTussenHaltes() {
        return tussenHaltes;
    }

    public void setTussenHaltes(ArrayList<String> tussenHaltes) {
        this.tussenHaltes = tussenHaltes;
    }


    public String getAankomstTijd() {
        return aankomstTijd;
    }

    public void setAankomstTijd(String aankomstTijd) {
        this.aankomstTijd = aankomstTijd;
    }

    public String getVertekTijd() {
        return vertekTijd;
    }


    public void setVertekTijd(String vertekTijd) {
        this.vertekTijd = vertekTijd;
    }

    public String getTijd() {
        return tijd;
    }

    public void setTijd(String tijd) {
        this.tijd = tijd;
    }

    public String getPerron() {
        return perron;
    }

    public String getStationNaam() {
        return stationNaam;
    }

    public String getPlaatsnaam() {
        return plaatsnaam;
    }

    public String getTraject() {
        return traject;
    }
}
