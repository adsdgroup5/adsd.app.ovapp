package adsd.app.ovapp.trein;

import adsd.app.ovapp.Functions;
import adsd.app.ovapp.Profile;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.regex.Pattern;
public class TreinDataModel {

    private static Document document;
    static NavigableMap<Integer, String[]> map;
    private static TreeMap<LocalTime, NavigableMap<Integer, String[]>> treemapTime;
    private static int interval;
    private static TreeMap<String, List<String>> targetMap;
    private static ArrayList<ArrayList<String>> allDelaysList;

    public static TreeMap<String, List<String>> getTimeTableFromJson(String stationVan, String stationNaar, String userTime) {

        targetMap = new TreeMap<String, List<String>>();

        try (Reader reader = new FileReader(
                Functions.getPathToJson ("arrivals.json"))) {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(reader);
            JSONArray response = (JSONArray) jsonObject.get("arrivals");

            // convert JsonArray to Array
            for (int i = 0; i < response.size(); i++) {
                JSONObject intercity = (JSONObject) response.get(i);
                String delay = (String) intercity.get("delay");
                JSONArray stations = (JSONArray) intercity.get("intercity");

                List<String> resultList = new ArrayList<String>();
                for (int j = 0; j < stations.size(); j++) {
                    resultList.add(stations.get(j).toString());
                }

                // search with regular expressions, regex for time:
                String regexStationVan = stationVan + "->" + "[0-9][0-9]:[0-9][0-9]";
                String regexStationNaar = stationNaar + "->" + userTime;
                Pattern patternVanStation = Pattern.compile(regexStationVan, Pattern.CASE_INSENSITIVE);
                Pattern patternNaarStation = Pattern.compile(regexStationNaar, Pattern.CASE_INSENSITIVE);
                boolean matchesStationVan = resultList.stream().anyMatch(v -> patternVanStation.matcher(v).matches());
                boolean matchesStationNaar = resultList.stream().anyMatch(v -> patternNaarStation.matcher(v).matches());

                if (matchesStationVan && matchesStationNaar) {
                    targetMap.put(delay, resultList);
                }
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }

        return  targetMap;

    }

    public static ArrayList<ArrayList<String>> getallDelaysList() {

        allDelaysList = new ArrayList<>();

        try (Reader reader = new FileReader(
                Functions.getPathToJson ("arrivals.json"))) {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(reader);
            JSONArray response = (JSONArray) jsonObject.get("arrivals");
            // convert JsonArray to Array
            for (int i = 0; i < response.size(); i++) {
                JSONObject intercity = (JSONObject) response.get(i);
                JSONArray stations = (JSONArray) intercity.get("intercity");
                String delay = (String) intercity.get("delay");
                // get Array of stations
                ArrayList<String> resultList = new ArrayList<String>();
                for (int j = 0; j < stations.size(); j++) {
                    resultList.add(stations.get(j).toString());

                }
                if (delay.equals("true")) {
                    allDelaysList.add(resultList);
                }
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }

        return allDelaysList;
    }

    public static TreeMap<LocalTime, NavigableMap<Integer, String[]>> getMapofTimes(String startStation, String endStation, String UserTime) {

        treemapTime = new TreeMap<>();

        Document document = getDoc("trein.xml");
        NodeList nodeList = document.getElementsByTagName("richting");

        String[] userTime = getUserTime(UserTime);
        int hour = Integer.parseInt(userTime[0]);
        int minute = Integer.parseInt(userTime[1]);

        //get start time from user, hours, minutes
        LocalTime startTime = LocalTime.of(hour, minute);

        for (int i = 0; i < nodeList.getLength(); i++) {
            map = new TreeMap<>();
            Node node = nodeList.item(i);
            Element lijnNode = (Element) node;
            NodeList listBusstops = lijnNode.getElementsByTagName("station");

            // search for the origin station in current node list
            for (int searchForStartStop = 0; searchForStartStop < listBusstops.getLength(); searchForStartStop++) {
                if (listBusstops.item(searchForStartStop).getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) listBusstops.item(searchForStartStop);
                    if (el.getTextContent().equals(startStation)) {
                        // search for the destination in current node list
                        for (int searchEndStop = 0; searchEndStop < listBusstops.getLength(); searchEndStop++) {
                            Element lastStopElement = (Element) listBusstops.item(searchEndStop);

                            if (lastStopElement.getTextContent().equals(endStation)) {
                                interval = interval + 15;
                                Node parent = lastStopElement.getParentNode().getParentNode();
                                Element parentElement = (Element) parent;
                                NodeList vertrekTijdList = parentElement.getElementsByTagName("vertrektijd");
                                for (int vertrektijd = 0; vertrektijd < vertrekTijdList.getLength(); vertrektijd++) {
                                    Node nodeVertrektijd = vertrekTijdList.item(vertrektijd);
                                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                                        Element element = (Element) node;
                                        // retrieve properties  station, distance, paltform
                                        String station = element.getElementsByTagName("station").item(vertrektijd).getTextContent();
                                        String afstand = element.getElementsByTagName("afstand").item(vertrektijd).getTextContent();
                                        String perron = element.getElementsByTagName("spoor").item(vertrektijd).getTextContent();
                                        // if the origin and destination stations found in a node list, put them in a map
                                        map.put(vertrektijd, new String[]{station, afstand, perron});                                            //interval=interval+15;
                                        treemapTime.put(startTime.plusMinutes(interval), map);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return treemapTime;
    }

    public static String[] getUserTime(String userTime) {
        String[] splitted = userTime.split(":");

        return splitted;
    }

    public static Document getDoc(String file) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse((TreinDataModel.class.getResourceAsStream("/resources/xml/"+file)));
            return document;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Profile>  getUsers() {
        ArrayList<Profile> userList = new ArrayList<Profile>();
        Document document = getDoc("gebruikers.xml");
        NodeList nodeList = document.getElementsByTagName("profile");

        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            Node nNode = nodeList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String voornaam=eElement.getElementsByTagName("voornaam").item(0).getTextContent();
                String achternaam=eElement.getElementsByTagName("acternaam").item(0).getTextContent();

                int leeftijd= Integer.parseInt(eElement.getElementsByTagName("leeftijd").item(0).getTextContent());

                String straatNaam=eElement.getElementsByTagName("straatNaam").item(0).getTextContent();
                String woonplaats=eElement.getElementsByTagName("woonplaats").item(0).getTextContent();

                int id= Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent());

                String closeToStation=eElement.getElementsByTagName("closeToStation").item(0).getTextContent();
                String closeToBusStop=eElement.getElementsByTagName("closeToBusStop").item(0).getTextContent();

                Profile profile=new Profile(voornaam,achternaam,leeftijd,straatNaam,woonplaats,id,closeToStation,closeToBusStop);

                userList.add(profile);

            }

        }

        return userList;
    }


    public static ArrayList<String[]>  getTrajecten() {

        ArrayList<String[]> favoritesList=new ArrayList<>();

        Document document = getDoc("trajecten.xml");
        NodeList nodeList = document.getElementsByTagName("traject");

        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            Node nNode = nodeList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String van=eElement.getElementsByTagName("van").item(0).getTextContent();
                String naar=eElement.getElementsByTagName("naar").item(0).getTextContent();
                String tijd=eElement.getElementsByTagName("tijd").item(0).getTextContent();
                String img=eElement.getElementsByTagName("img").item(0).getTextContent();
                favoritesList.add(new String[]{van,naar,tijd,img});

            }
        }

        return favoritesList;
    }
}