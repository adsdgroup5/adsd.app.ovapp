package adsd.app.ovapp.trein;

import adsd.app.ovapp.Functions;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;


public class Stations extends JPanel{

    private String[] defaultValuesVertrek =  {"Utrecht Centraal","Bilthoven","Enschede","Almelo","Hengelo","Apeldoorn","Apeldoorn Osseveld","Deventer Colmschate","Deventer","Amersfoort Centraal",
            "Utrecht Overvecht","Gouda","Gouda Zoetermeer", "Den Haag Centraal","Den Haag Ypenburg","Den Haag Voorburg"
    };

    private JList arrivalsList;
    private static String startStation;
    private static String endStation;
    private static ArrayList<String> intermediateStops;
    private static ArrayList<Double> result;
    private static  DefaultTableModel model;
    private static ResourceBundle chosenLanguage;
    private static boolean forwardDirection=true;
    private static String chosenTime="08:00";

    // first constructor is called, if the user travels from current location
    public Stations(String userLocation, String userName, boolean fromCurrentLocation,String lang, String country)
    {
        startStation=userLocation;
        getStartPanel(userLocation, userName, fromCurrentLocation,lang,country);
    }

   // second contructor is called, if the current location not defined
    public Stations( boolean fromCurrentLocation,String lang, String country) {

        getStartPanel(null,null,fromCurrentLocation,lang,country);

    }
    
    public Stations( String lang, String country) {
        getArrivals(lang,country);

    }



    private JPanel getStartPanel (String userLocation, String userName, boolean fromCurrentLocation, String lang, String country)
    {
        String[] timesString = new String[] { "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00",
                "11:30", "12:00","12:30","13:00","13:30", "14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30",
                "19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","00:00"};

        lang=lang;
        country=country;
        Locale.setDefault(new Locale(lang, country));
        chosenLanguage = ResourceBundle.getBundle("bundle");
        
        JPanel startPanel=new JPanel();
        JComboBox<String> timeSelect = new JComboBox<>(timesString);
        arrivalsList = createJList(defaultValuesVertrek);
        
        JList destinantionList = createJList(defaultValuesVertrek);
        String startString = chosenLanguage.getString("kies_uw_startstation");
        
        JLabel halloUserLabel=new JLabel("Hallo, "+userName);
        JLabel yourNearestStation=new JLabel(chosenLanguage.getString("uw_startstation_is"));
        JLabel yourLocation=new JLabel(userLocation); setLink(yourLocation);
        setFont(yourNearestStation); setFont(yourLocation); setFont(halloUserLabel);
        
        JLabel startLabel=new JLabel(startString);
        JTextField startTextField=createTextField(arrivalsList,defaultValuesVertrek);
        startTextField.setVisible(fromCurrentLocation?false:true);
        String endString = fromCurrentLocation==false ? chosenLanguage.getString("kies_uw_eindstation") : chosenLanguage.getString("waar_wilt_u_heen");
        JLabel endLabel=new JLabel(endString); setFont(endLabel);
        
        JTextField destinationTextFiled=createTextField(destinantionList,defaultValuesVertrek);
        JPanel leftPanel=new JPanel();
        add(leftPanel);
        JPanel rigthtPanel=new JPanel();
        add(rigthtPanel);
        rigthtPanel.setBorder(new LineBorder(Color.LIGHT_GRAY));
        
        leftPanel.setPreferredSize(new Dimension(225, 455));
        leftPanel.setBorder(new LineBorder(Color.lightGray));
        leftPanel.setLayout(new GridLayout(0, 1));
        
        rigthtPanel.setPreferredSize(new Dimension(424,455));
        JPanel textPanel=new JPanel();
        JPanel destinationTextFieldPanel=new JPanel();
        
        Color blueColor =new Color(197,225,250);
        textPanel.setBackground(blueColor);
        destinationTextFieldPanel.setBackground(blueColor);
        
        leftPanel.add(fromCurrentLocation?halloUserLabel:textPanel);
        leftPanel.add(fromCurrentLocation?yourNearestStation:startTextField);       
        textPanel.add(startTextField);textPanel.add(startLabel);textPanel.add(timeSelect);
        
        startTextField.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        destinationTextFiled.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        JScrollPane departScroll=new JScrollPane(arrivalsList);
        
        leftPanel.add(fromCurrentLocation?yourLocation:departScroll);
        leftPanel.add(endLabel); leftPanel.add(destinationTextFiled);

        JScrollPane destinationScroll=new JScrollPane(destinantionList);
        destinationTextFieldPanel.add(destinationTextFiled); destinationTextFieldPanel.add(endLabel);
        
        leftPanel.add(destinationTextFieldPanel);
        leftPanel.add(destinationScroll);

        ImageIcon reverse = new ImageIcon(Functions.getUrl("reverse.png"));
        JLabel reverseLabel = new JLabel(reverse);
        reverseLabel.setVisible(fromCurrentLocation?false:true);
        leftPanel.add(reverseLabel);
        reverseLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        reverseLabel.setForeground(Color.BLUE.darker());

        timeSelect.addItemListener((ItemEvent e) -> {
            Object item = e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                // Item has been selected
                chosenTime=(String) item;
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                // Item has been deselected
                chosenTime=null;

            }
        });

        leftPanel.add (new JButton(new AbstractAction(chosenLanguage.getString("plan_uw_rijs")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateTextField(startTextField,destinationTextFiled,fromCurrentLocation);
                try {
                    // define start station. if user travels not from current location, get start station from text filed
                    startStation=fromCurrentLocation?userLocation:startTextField.getText();
                    endStation=destinationTextFiled.getText();

                    //get all direction with given origin and destination stations
                    TreeMap<LocalTime, NavigableMap<Integer,String[]>>allDirection=getAllDirection(startStation,endStation,chosenTime);
                    for (Map.Entry<LocalTime, NavigableMap<Integer,String[]>> entry : allDirection.entrySet()) {

                        LocalTime key = entry.getKey();
                        NavigableMap<Integer,String[]> value= entry.getValue();
                        JTree tree=createTree(startStation, endStation, value,key);
                        rigthtPanel.add(tree);
                        rigthtPanel.validate();
                        rigthtPanel.repaint();

                    }
                }

                catch(NullPointerException err)
                {
                    //System.out.print("station not found");
                }
            }
        }));



// mouse listener
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 1) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        startTextField.setText(o.toString());

                    }
                }
            }
        };

        //add mouse listener
        arrivalsList.addMouseListener(mouseListener);

        MouseListener destinantionListListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 1) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        destinationTextFiled.setText(o.toString());

                    }
                }
            }
        };

        //add mouse listener
        destinantionList.addMouseListener(destinantionListListener);


        //mouse listener reverse label
        reverseLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                String startText=startTextField.getText();
                String destinationText=destinationTextFiled.getText();
                startTextField.setText(destinationText);
                destinationTextFiled.setText(startText);
            }
        });
        return startPanel;
    }

    private JTree createTree (String startStation, String endStation, NavigableMap<Integer,String[]> map, LocalTime userTime) {

        String vertrektijd= String.valueOf(userTime);

        int startStationIndex=getKey(map, getMapValue(map, startStation));
        int endStationIndex=getKey(map, getMapValue(map, endStation));

        String[] startStationMapValue= getMapValue (map,startStation);
        String[] endStationMapValue= getMapValue (map,endStation);
        String direction=getDirection(startStationIndex,endStationIndex,map);
        String perron=getPerron(endStationMapValue);
        ArrayList<String> intermediateStops=getIntermediateStops(startStationIndex,endStationIndex,map);

        double distance=getDistance(startStationIndex,endStationIndex,map);

        TreinTijd treinTijdObject=new TreinTijd(perron,direction,distance,intermediateStops);
        treinTijdObject.setVertekTijd(vertrektijd);

        String aankomstTijd=String.valueOf(calculateTime(distance,vertrektijd));
        treinTijdObject.setAankomstTijd(aankomstTijd);

        DefaultMutableTreeNode parentNode=new DefaultMutableTreeNode(chosenLanguage.getString("uw_reis")+startStation+"-"+endStation);
        DefaultMutableTreeNode directionNode=new DefaultMutableTreeNode(chosenLanguage.getObject("intercity_richting")+treinTijdObject.getTraject());
        DefaultMutableTreeNode spoorNode=new DefaultMutableTreeNode(chosenLanguage.getString("spoor") +treinTijdObject.getPerron());
        DefaultMutableTreeNode distanceNode=new DefaultMutableTreeNode(chosenLanguage.getString("afstand_km") +treinTijdObject.getAfstand() + " km");
        DefaultMutableTreeNode timeNode=new DefaultMutableTreeNode(chosenLanguage.getObject("vertrektijd")+treinTijdObject.getVertekTijd());
        DefaultMutableTreeNode arrivalTimeNode=new DefaultMutableTreeNode(chosenLanguage.getString("aankomsttijd")+treinTijdObject.getAankomstTijd());
        DefaultMutableTreeNode stations=new DefaultMutableTreeNode(chosenLanguage.getString("Toon_tussenstops"));
        ArrayList<String> allStations=treinTijdObject.getTussenHaltes();

        //add intermediate stations to tree:
        allStations.stream().forEach(station->stations.add(new DefaultMutableTreeNode(station)));
        parentNode.add(directionNode); parentNode.add(spoorNode); parentNode.add(distanceNode);parentNode.add(timeNode);parentNode.add(arrivalTimeNode);parentNode.add(stations);
        Icon icon = new ImageIcon(Functions.getUrl("info.png"));
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
        renderer.setOpenIcon(icon);
        renderer.setClosedIcon(icon);
        renderer.setLeafIcon(icon);
        final JTree tree = new JTree(parentNode);
        tree.setCellRenderer(renderer);
        return tree;
    }

    private JTextField createTextField(JList list, String[] defaultValues) {
        final JTextField field = new JTextField(15);
        field.getDocument().addDocumentListener(new DocumentListener(){
            @Override public void insertUpdate(DocumentEvent e) { filter(); }
            @Override public void removeUpdate(DocumentEvent e) { filter(); }
            @Override public void changedUpdate(DocumentEvent e) {}
            private void filter() {
                String filter = field.getText();
                filterModel((DefaultListModel<String>)list.getModel(), filter, defaultValues);
            }
        });
        return field;
    }

    private JList createJList(String[] defaultValues) {
        JList list = new JList(createDefaultListModel( defaultValues));
        list.setVisibleRowCount(6);
        return list;
    }

    private ListModel<String> createDefaultListModel(String[] defaultValues) {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (String s : defaultValues) {
            model.addElement(s);
        }
        return model;
    }

    public void filterModel(DefaultListModel<String> model, String filter,String[] defaultValues ) {
        for (String s : defaultValues) {
            if (!s.startsWith(filter)) {
                if (model.contains(s)) {
                    model.removeElement(s);
                }
            } else {
                if (!model.contains(s)) {
                    model.addElement(s);
                }
            }
        }
    }

    public static double getDistance (int startStationIndex, int endStationIndex, NavigableMap<Integer, String[]> map)
    {
        result = new ArrayList<Double>();
        //calculate distance for ascending direction
        if (forwardDirection)
        {
            NavigableMap<Integer, String[]> submap = map.subMap(startStationIndex, false, endStationIndex, true);

            Double sumVal = submap.entrySet().stream()
                    .map(x->x.getValue()[1])
                    .mapToDouble(num->Double.parseDouble(num))
                    .sum();

            return sumVal;

        }
        else
        //calcuate distace for inverse direction, reverse order, using descendingMap()
        {
            NavigableMap<Integer, String[]> submap = map.descendingMap().subMap(startStationIndex, true, endStationIndex, false);
            Double sumVal = submap.entrySet().stream()
                    .map(x->x.getValue()[1])
                    .mapToDouble(num->Double.parseDouble(num))
                    .sum();

            return sumVal;
        }

    }

    public static ArrayList<String> getIntermediateStops(int startStationIndex, int endStationIndex, NavigableMap<Integer, String[]> map)
    {


        intermediateStops = new ArrayList<String>();

        if (forwardDirection)
        {

            NavigableMap<Integer, String[]> submap = map.subMap(startStationIndex, true, endStationIndex, true);
            ArrayList<String> intermediateStops=submap.entrySet().stream()
                    .map(x->x.getValue()[0])
                    .collect(Collectors
                            .toCollection(ArrayList::new));
            return intermediateStops;
        }
        // for backward direction inverse map, using descendingMap() built in function
        else
        {
            NavigableMap<Integer, String[]> submap = map.descendingMap().subMap(startStationIndex, true, endStationIndex, true);
            ArrayList<String> intermediateStops=submap.entrySet().stream()
                    .map(x->x.getValue()[0])
                    .collect(Collectors
                            .toCollection(ArrayList::new));
            return intermediateStops;
        }

    }

    public LocalTime calculateTime(double afstand, String userStartTime)
    {

        // het reistijd wordt berekent met gemiddelde snelheid 0,02 km/sec

        double v=0.02;
        String[] splitted=Functions.getUserTime(userStartTime);
        int hour= Integer.parseInt(splitted[0]);
        int min= Integer.parseInt(splitted[1]);
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        long reistijdInSeconds = Math.round(afstand / v);
        LocalTime startTime = LocalTime.of(hour,min);
        LocalTime reisTijd=startTime.plusSeconds(reistijdInSeconds);
        return reisTijd;

    }
    public  String[] getMapValue  (NavigableMap<Integer, String[]> map, String valueStation) {

        String[] resultSet=map.entrySet().stream()
                .map(x->x.getValue())
                .filter(x->valueStation.equals(x[0])).findAny().orElse(null);

        return resultSet;

    }

    public static <T, E> T getKey (NavigableMap<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                // get the key of the stations in a map
                return entry.getKey();
            }
        }
        return null;
    }

    public static String getDirection(int startStationIndex,int endStationIndex, NavigableMap <Integer, String[]> map)
    {
        // define direction by compare last en first stop indexes
        forwardDirection=(endStationIndex>startStationIndex)?true:false;
        List<String[]> values = map.entrySet().stream()
                .map(x->x.getValue())
                .collect(Collectors.toList());
        // retireve the start end end stops from data
        String  directionDescending=values.get(0)[0];
        //get last city:
        int size=values.size()-1;
        String directionAscending=values.get(size)[0];
        String direction=forwardDirection?directionAscending:directionDescending;
        return direction;
    }

    public static String  getPerron(String[] endStationInfo)
    {
        return endStationInfo[2];
        // perron is the third elemeny in array with all the information
    }

    // array list of maps
    public TreeMap<LocalTime, NavigableMap<Integer,String[]>> getAllDirection(String startStation,String endStation, String UserTime)
    {
        TreinDataModel datamodel=new TreinDataModel();
        TreeMap<LocalTime, NavigableMap<Integer,String[]>>allDirection=datamodel.getMapofTimes(startStation,endStation, UserTime);
        return allDirection;
    }

    public void  setFont(JLabel label)
    {
        label.setFont(new Font("verdana", Font.PLAIN, 12));
    }
    public void setLink(JLabel link) {
        link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); link.setForeground(Color.BLUE.darker());
    }

    public JPanel getArrivals(String lang, String country)
    {
        TreinDataModel datamodel=new  TreinDataModel();
        //datamodel.getVertraging("10:00","Amsterdam Centraal","Hilversum");
        Locale.setDefault(new Locale(lang, country));
        String[] defaultValuesAankomst =  {"Amsterdam Centraal", "Utrecht Centraal", "Hilversum", "Amersfoort Centraal", "Deventer"};
        JPanel arrivalPanel=new JPanel();
        setLayout(new BorderLayout());
        Locale.setDefault(new Locale(lang, country));
        ResourceBundle chosenLanguage = ResourceBundle.getBundle("bundle");
        JPanel rigthtPanelanel=new JPanel();
        JPanel leftPanel=new JPanel();
        JPanel leftPanelanelTop=new JPanel();
        SimpleDateFormat formatter=new SimpleDateFormat("HH:mm, dd/MM/yyyy");
        Date date =new Date(System.currentTimeMillis());
        JLabel currentTime=new JLabel(formatter.format(date)); setFont(currentTime);
        leftPanelanelTop.add(currentTime);
        JLabel infoLbl=new JLabel(chosenLanguage.getString("Info_over_aankomsttijden"));setFont(infoLbl);
        leftPanelanelTop.add(infoLbl);
        String[] timesString = new String[] { "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00",
                "11:30", "12:00","12:30","13:00","13:30", "14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30",
                "19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","00:00"};


        JPanel startPanel=new JPanel();
        JComboBox<String> timeSelect = new JComboBox<>(timesString);
        JList arrivalListFrom = createJList(defaultValuesAankomst);
        JList arrivalsList = createJList(defaultValuesAankomst);
        String startString = chosenLanguage.getString("kies_uw_startstation");
        JLabel startLabel=new JLabel(startString);
        JTextField startTextField=createTextField(arrivalListFrom,defaultValuesAankomst);
        startTextField.setVisible(true);
        String endString = chosenLanguage.getString("uw_aankomst_op");
        JLabel endLabel=new JLabel(endString); setFont(endLabel);
        JTextField destinationTextFiled=createTextField(arrivalsList,defaultValuesAankomst);

        JPanel rigthtPanel=new JPanel();
        add(rigthtPanel);

        rigthtPanel.setBorder(new LineBorder(Color.LIGHT_GRAY));
        leftPanel.setPreferredSize(new Dimension(225, 455));
        leftPanel.setBorder(new LineBorder(Color.lightGray));
        leftPanel.setLayout(new GridLayout(0, 1));

        rigthtPanel.setPreferredSize(new Dimension(524,455));
        JPanel textPanel=new JPanel();
        JPanel destinationTextFieldPanel=new JPanel();

        Color blueColor =new Color(197,225,250);
        textPanel.setBackground(blueColor);
        destinationTextFieldPanel.setBackground(blueColor);
        leftPanel.add(leftPanelanelTop);

        leftPanel.add(textPanel);
        leftPanel.add(startTextField);
        textPanel.add(startTextField);textPanel.add(startLabel);
        JScrollPane departScroll=new JScrollPane(arrivalListFrom);

        leftPanel.add(departScroll);
        leftPanel.add(endLabel); leftPanel.add(destinationTextFiled);

        JScrollPane destinatioScroll=new JScrollPane(arrivalsList);
        destinationTextFieldPanel.add(destinationTextFiled); destinationTextFieldPanel.add(endLabel);destinationTextFieldPanel.add(timeSelect);

        leftPanel.add(destinationTextFieldPanel);
        leftPanel.add(destinatioScroll);

        Border line=BorderFactory.createLineBorder(Color.DARK_GRAY,1);
        add(arrivalPanel); arrivalPanel.add(leftPanel); arrivalPanel.add(rigthtPanel);

        JPanel tablePanel=new JPanel();
        String[][] data={};

        String column[]={chosenLanguage.getString("aankomstijd"),chosenLanguage.getString("traject"),  chosenLanguage.getString("aankomst_bestemming")};
        JTable arrivalTable=new JTable();
        arrivalTable.setModel(new DefaultTableModel(data, column));
        model = (DefaultTableModel)arrivalTable.getModel();

        arrivalTable.getColumnModel().getColumn(0).setPreferredWidth(17);
        arrivalTable.getColumnModel().getColumn(1).setPreferredWidth(127);
        arrivalTable.getColumnModel().getColumn(2).setPreferredWidth(127);

        JScrollPane scrollPane=new JScrollPane(arrivalTable);
        scrollPane.setPreferredSize(new Dimension(500,200));
        tablePanel.add(scrollPane); rigthtPanel.add(tablePanel);

        MouseListener arrivalListFromListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 1) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        startTextField.setText(o.toString());
                    }
                }
            }
        };
        MouseListener arrivalListListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 1) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        destinationTextFiled.setText(o.toString());
                    }
                }
            }
        };

        //add mouse listener
        arrivalListFrom.addMouseListener(arrivalListFromListener);
        arrivalsList.addMouseListener(arrivalListListener);

        timeSelect.addItemListener((ItemEvent e) -> {
            Object item = e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                // Item has been selected
                chosenTime=(String) item;
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                // Item has been deselected
                chosenTime=null;
            }
        });

        leftPanel.add (new JButton(new AbstractAction(chosenLanguage.getString("zoek_aankomsttijden")) {
            @Override
            public void actionPerformed(ActionEvent e) {

                if  (startTextField.getText().isEmpty() || destinationTextFiled.getText().isEmpty())
                {
                    JOptionPane.showMessageDialog(null, chosenLanguage.getString("empty_imput"));
                    return;
                }
                else
                {

                    try {
                        TreinDataModel dataModel=new TreinDataModel();
                        startStation=startTextField.getText();
                        endStation=destinationTextFiled.getText();
                        String selectedTime=chosenTime;
                        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
                        TreeMap<String, List<String>> targetMap=dataModel.getTimeTableFromJson(startStation,endStation,selectedTime);

                        if (targetMap.isEmpty()) {
                            System.out.println("empty!");
                            JOptionPane.showMessageDialog(null, "er zijn rond deze tijd geen treinen beschikbaar naar uw bestemming, probeer een andere bestemming");

                        }

                        else {
                            for (Map.Entry<String,List<String>> entry : targetMap.entrySet()) {
                                //System.out.println("map Key = " + entry.getKey() + ", map Value = " + entry.getValue());
                                Boolean delay= Boolean.valueOf(entry.getKey());
                                String intercityFrom=entry.getValue().get(0);
                                int size=(entry.getValue().size()-1);
                                String intercityTo=entry.getValue().get(size);
                                model.addRow(new String[]{selectedTime,startTextField.getText()+"-"+destinationTextFiled.getText(),intercityTo});

                                if (delay==true) {
                                    JOptionPane.showMessageDialog(null, chosenLanguage.getString("De_intercity_tussen") + " " +entry.getValue().get(0)+ " "+chosenLanguage.getString("en")+ entry.getValue().get(size-1)+" " +chosenLanguage.getString("heeft_vertraging"));

                                }
                            }
                        }
                    }

                    catch(NullPointerException err) {
                        //System.out.print("station not found");
                    }
                }
            }
        }));

        return arrivalPanel;

    }

    //add mousLstener
    public JPanel getTable(String userTime, String startStation, String endStation, String lang, String country) {
        JPanel tablePanel=new JPanel();
        return tablePanel;
    }


    public boolean validateTextField(JTextField startTextField, JTextField destinationTextFiled, boolean fromCurrentLocation) {
        boolean validation;
        // check if the text field not empty
        if (fromCurrentLocation &&(destinationTextFiled.getText().isEmpty())) {
            JOptionPane.showMessageDialog(null, chosenLanguage.getString("empty_imput"));
            validation=false;
            return validation;
        }

        else if (fromCurrentLocation==false &&(startTextField.getText().isEmpty() || destinationTextFiled.getText().isEmpty())) {
            JOptionPane.showMessageDialog(null, chosenLanguage.getString("empty_imput"));
            validation=false;
            return validation;
        }

        else if (!startTextField.getText().isEmpty()&&!destinationTextFiled.getText().isEmpty()) {
            validation=true;
            return validation;
        }

        return false;
    }
}