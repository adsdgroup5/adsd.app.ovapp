package adsd.app.ovapp.UnitTests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Tester {
    public static void main(String[] args) {

        System.out.println("test is running...");

        Result result = JUnitCore.runClasses(adsd.app.ovapp.UnitTests.JunitTest.class);

        for (Failure failure : result.getFailures())
        {
            System.out.println(failure.toString());
        }

        System.out.println("\n[!] Overall test result: " + result.wasSuccessful() + "\n");

    }
}
