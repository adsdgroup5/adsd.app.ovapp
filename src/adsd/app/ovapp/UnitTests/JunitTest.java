package adsd.app.ovapp.UnitTests;

import adsd.app.ovapp.Profile;
import adsd.app.ovapp.trein.TreinDataModel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

public class JunitTest {

    @Test
    public void testGetTimeTableFromJson(){

        String actual = TreinDataModel.getTimeTableFromJson("Hilversum","Schiphol Airport","19:00").toString();
        String expected="{true=[Utrecht Centraal->17:30, Hilversum->18:00, Amsterdam Centraal->18:30, Schiphol Airport->19:00, Leiden Central->19:30]}";
        assertEquals(actual,expected);
        System.out.println("test function GetTimeTableFromJson() is successful");
    }

    @Test
    public void testFunctiongetDelays() {

       String[] actual= TreinDataModel.getallDelaysList().get(0).toArray(new String[0]);
       String[] expected={"Apeldoorn->09:00", "Utrecht Centraal->09:30", "Naarden-Bussum->10:00", "Amsterdam Centraal->10.30"};
       assertArrayEquals(expected, actual);
       System.out.println("test function testFunctiongetDelays() delay is successful");

    }

    @Test
    public void testgetTrajecten() {

        String[] expected={"Amsterdam Centraal","Hilversum","09:30","map1.png"};
        String[] actual=TreinDataModel.getTrajecten().get(1);
        assertArrayEquals(expected, actual);
        System.out.println("test function testgetTrajecten()  is successful");

    }

    @Test
    public void testGetUsers() {

         Profile profile=new Profile ("Anton","Van den Berg",23,"Mozartweg","Amersfoort",2,"Amersfoort Centraal","Amersfoort, Nijenstede");

         String expectedName=profile.getVoorNaam();
         String expectedSurname=profile.getAchterNaam();
         String expectedResidence=profile.getWoonplaats();
         String expectedId=String.valueOf(profile.getId());
         Profile actual=TreinDataModel.getUsers().get(1);

         String actualName=actual.getVoorNaam();
         String actualSurname=actual.getAchterNaam();
         String actualResidence=actual.getWoonplaats();
         String actualId=String.valueOf(actual.getId());

         assertEquals(expectedName, actualName);
         assertEquals(expectedSurname, actualSurname);
         assertEquals(expectedResidence, actualResidence);
         assertEquals(expectedId, actualId);

        System.out.println("test function testGetUsers is successful");

    }
}

