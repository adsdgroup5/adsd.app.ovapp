package adsd.app.ovapp;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Predicate;

public class Login extends JFrame  {

    private String language = "nl";
    private String country = "NL";
    private boolean isAdmin = false;
    private String userName;
    private int userId;
    private ResourceBundle chosenLanguage;

    public Login (ArrayList<Profile> profileList) {
        JFrame loginFrame = new JFrame("OV App Login");

        JLabel headingsLabel, userNameLabel, userPasswordLabel;

        headingsLabel = new JLabel("Welkom!");
        headingsLabel.setForeground(Color.blue);
        headingsLabel.setFont(new Font("Serif", Font.BOLD, 20));
        headingsLabel.setBounds(50, 30, 400, 30);

        userNameLabel = new JLabel("Gebruikersnaam");
        userNameLabel.setBounds(50, 70, 200, 30);

        userPasswordLabel = new JLabel("Wachtwoord");
        userPasswordLabel.setBounds(50, 110, 200, 30);

        JTextField userNameTextField = new JTextField();
        userNameTextField.setBounds(160, 70, 160, 30);
        userNameTextField.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPasswordField passwordTextField = new JPasswordField();
        passwordTextField.setBounds(160, 110, 160, 30);
        passwordTextField.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JButton loginButton = new JButton("Login");
        loginButton.setBounds(220, 160, 100, 30);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Locale.setDefault(new Locale(language, country));
                chosenLanguage = ResourceBundle.getBundle("bundle");
                userName = userNameTextField.getText();
                String passWord = String.valueOf(passwordTextField.getPassword());
                getAuthorized(userName, passWord, loginFrame, profileList);

            }
        });

        JPanel taal = new JPanel();
        taal.setBounds(0,300,400,50);

        JButton englishButton = new JButton("EN");
        englishButton.setIcon(new ImageIcon(Functions.getUrl("usa.png")));
        englishButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                language ="en";
                country="US";

            }
        });

        JButton dutchButton = new JButton("NL");
        dutchButton.setIcon(new ImageIcon(Functions.getUrl("flagnl.png")));
        dutchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                language ="nl";
                country ="NL";

            }
        });

        taal.add(dutchButton);
        taal.add(englishButton);
        loginFrame.add(taal);
        loginFrame.add(headingsLabel);
        loginFrame.add(userNameLabel);
        loginFrame.add(userNameTextField);
        loginFrame.add(userPasswordLabel);
        loginFrame.add(passwordTextField);
        loginFrame.add(loginButton);

        loginFrame.setSize(400, 400);
        loginFrame.setLayout(null);
        loginFrame.setVisible(true);
        loginFrame.setLocationRelativeTo(null);
        Image icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(Functions.getUrl("icon2.png"))).getImage();
        loginFrame.setIconImage(icon);
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        englishButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Border etched = BorderFactory.createEtchedBorder(3,Color.DARK_GRAY,Color.DARK_GRAY);
                englishButton.setBorder(etched);

            }
            @Override
            public void mouseEntered(MouseEvent e) {
                // the mouse has entered the button
                Border etched = BorderFactory.createMatteBorder(4, 4, 4, 4, Color.BLUE);
                englishButton.setBorder(etched);
            }
            @Override
            public void mouseExited(MouseEvent e) {
                // the mouse has exited the button
                englishButton.setBorder(null);
            }
        });

        dutchButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // the user clicks on the label
                Border etched = BorderFactory.createEtchedBorder(3,Color.DARK_GRAY,Color.DARK_GRAY);
                dutchButton.setBorder(etched);

            }
            @Override
            public void mouseEntered(MouseEvent e) {
                // the mouse has entered the label
                Border etched = BorderFactory.createMatteBorder(4, 4, 4, 4, Color.BLUE);
                dutchButton.setBorder(etched);
            }
            @Override
            public void mouseExited(MouseEvent e) {
                // the mouse has exited the label
                dutchButton.setBorder(null);
            }
        });
    }

    public void getAuthorized(String userName, String passWord, JFrame loginFrame, ArrayList<Profile> profileList) {
        // first check if the logged user is Administrator
        isAdmin = userName.equalsIgnoreCase("admin") && passWord.equals("password");
        if(isAdmin) {
            isAdmin=true;
            this.userName ="Admin";
            userId=1;
            JOptionPane.showMessageDialog(null, chosenLanguage.getString("Hallo") + ", " + this.userName + "! "+chosenLanguage.getString("ingelogged"), chosenLanguage.getString("login_message"), 1);
            DefaultFrame defaultFrame = new DefaultFrame(profileList, language, country, userId, isAdmin);
            loginFrame.dispose();
            return;
        }

        // authorization, check if the user is registered
        Predicate<Profile> predicate = profile -> profile.getVoorNaam().equalsIgnoreCase(userName) && passWord.equals("password");
        boolean authorization = profileList.stream().anyMatch(predicate);
        if(authorization) {
            this.userName = userName;
            Profile userProfile = profileList.stream().filter(x->userName.equalsIgnoreCase(x.getVoorNaam())).findAny().orElse(null);
            JOptionPane.showMessageDialog(null,  chosenLanguage.getString("Hallo") +", "+userProfile.getVoorNaam()+" !"+chosenLanguage.getString("ingelogged"));
            DefaultFrame defaultFrame = new DefaultFrame(profileList, language, country, userProfile.getId(), isAdmin);
            loginFrame.dispose();
            return;
        }

        // if the user is not authorized
        JOptionPane.showMessageDialog(loginFrame, chosenLanguage.getString("incorrect_username"),
                "Error",JOptionPane.ERROR_MESSAGE);
    }

}