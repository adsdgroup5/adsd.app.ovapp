package adsd.app.ovapp.bus;

import java.util.ArrayList;

public class BusTijd {

    private String arrivalTime;
    private String departureTime;
    private String line;
    private String startStop;
    private String endStop;
    private String cityName;
    private String traject;
    private String time;
    private double distance;
    private double price;
    private String direction;
    private ArrayList<String> stops;

    //constructor with overloading
    public BusTijd()
    {
        this(null, null,null, null,null,0.0, 0.0,null,null);
    }
    
    public BusTijd(String line, String startStop, String endStop, String direction, double distance, ArrayList<String> stops)    {
        this(line, startStop,endStop,null, null, distance,0.0, direction, stops);
    }

    public BusTijd(String line, String startStop, String endStop, String cityName, String traject, double distance, double price, String direction, ArrayList<String> stops) {
        this.line = line;
        this.startStop = startStop;
        this.endStop = endStop;
        this.cityName = cityName;
        this.traject = traject;
        this.distance = distance;
        this.price = price;
        this.direction = direction;
        this.stops = stops;
    }

    public String getStartStop() {
        return startStop;
    }

    public void setStartStop(String startStop) {
        this.startStop = startStop;
    }

    public String getEndStop() {
        return endStop;
    }

    public void setEndStop(String endStop) {
        this.endStop = endStop;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getlijn() {
        return line;
    }

    public String getCityName() {
        return cityName;
    }

    public String getTraject() {
        return traject;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setTraject(String traject) {
        this.traject = traject;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public ArrayList<String> getStops() {
        return stops;
    }

    public void setStops(ArrayList<String> stops) {
        this.stops = stops;
    }
}
