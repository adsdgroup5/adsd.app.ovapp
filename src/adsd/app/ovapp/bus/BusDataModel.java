package adsd.app.ovapp.bus;

import adsd.app.ovapp.Functions;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.LocalTime;
import java.util.NavigableMap;
import java.util.TreeMap;

public class BusDataModel {

    private static TreeMap<LocalTime, NavigableMap<Integer,String>> treeMapOfTimes;
    private static int  interval;
    private static Document document;

    public TreeMap<LocalTime, NavigableMap<Integer,String>> getMapofTimes(String endStop, String selectedTime) {
        NavigableMap<Integer, String> map = new TreeMap<Integer, String>();
        String[] userTime = Functions.getUserTime(selectedTime);

        int hour = Integer.parseInt(userTime[0]);
        int minute = Integer.parseInt(userTime[1]);

        //get start time from user, hours, minutes
        LocalTime startTime = LocalTime.of(hour, minute);
        Document document = getDoc("bus.xml");
        NodeList nodeList = document.getElementsByTagName("lijn");

        // Iterate through xml data nodes
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            NodeList list = node.getChildNodes();
            for (int j = 0; j < list.getLength(); j++) {
                Node BushalteNode = list.item(j);
                if (BushalteNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (endStop.equals(BushalteNode.getTextContent())) {
                        interval = interval + 15;
                        Node parentNode = BushalteNode.getParentNode();
                        NodeList childNodeList = parentNode.getChildNodes();
                        for (int c = 0; c < childNodeList.getLength(); c++) {
                            Node currentNode = childNodeList.item(c);
                            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                                String station = currentNode.getTextContent();
                                map.put(c, station);
                            }
                        }
                    }
                }
            }
        }

        treeMapOfTimes = new TreeMap<>();
        treeMapOfTimes.put(startTime.plusMinutes(interval), map);
        return treeMapOfTimes;
    }

    public static String getLine(String endStop) {
        Document document=getDoc("bus.xml");
        NodeList nodeList = document.getElementsByTagName("lijn");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            NodeList list = node.getChildNodes();
            for (int j = 0; j < list.getLength(); j++) {
                Node BushalteNode = list.item(j);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    if (endStop.equals(BushalteNode.getTextContent())) {
                        Node parentNode = BushalteNode.getParentNode();
                        String id = parentNode.getAttributes().getNamedItem("id").getNodeValue();
                        String line = id;
                        return line;
                    }
                }
            }
        }
        return null;
    }

    public static Document getDoc(String file) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse((BusDataModel.class.getResourceAsStream("/resources/xml/"+file)));
            return document;
        }
        catch (ParserConfigurationException | SAXException  |  IOException e)
        {e.printStackTrace();} return null;
    }
}