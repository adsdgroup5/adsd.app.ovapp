package adsd.app.ovapp.bus;

import adsd.app.ovapp.Functions;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.time.LocalTime;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BusStops extends JPanel {

    private String[] haltesAmersfoort = {"Leusden, Leusbroekerweg/De Mof", "Leusden, Karel van Ginkelstraat", "Leusden, Karel van Ginkelstraat", "Leusden, Maximaplein", "Leusden, Burg. Buiningpark", "Leusden, Groene Zoom",
            "Leusden, Heiligenbergerweg", "Leusden, Ursulineweg", "Amersfoort, Haydnstraat", "Amersfoort, Nijenstede","Amersfoort, Boreelstraat", "Amersfoort, Blekerssingel",
            "Amersfoort, Hendrik van Viandenstraat", "Amersfoort, De Kei", "Amersfoort, Centrum", "Amersfoort, Snouckaertlaan", "Amersfoort, Centraal Station"
    };

    private String[] haltesDeventer = { "Station,Deventer","Schouwburg/Centrum,Deventer","Beestenmarkt,Deventer","Bleekstraat,Deventer","Veenweg,Deventer","Fesevurstraat,Deventer",
            "Bekkumer,Deventer","Ziekenhuis,Deventer","Wijtenhorst,Deventer","Cornelis Lelylaan,Deventer","Willem Witsenstraat,Deventer",
            "Alphons Diepenbrocklaan,Deventer","Albertus van Leusenweg,Deventer","David Wijnveldtweg,Deventer","Oostriklaan,Deventer",
            "Station Colmschate,Deventer","De Scheg,Deventer","Fonteinkruid,Deventer","Winkelcentrum Colmschate,Deventer" };

    private String[] haltesUtrecht = {"Centrumzijde, Utrecht","Sterrenwijk, Utrecht","Rubenslaan, Utrecht","Diakonessenhuis-Zuid, Utrecht","Rijnsweerd-Noord, Utrecht","Rijnsweerd-Zuid, Utrecht","Botanische Tuinen, Utrecht",
            "Heidelberglaan, Utrecht","UMC Utrecht, Utrecht","WKZ/Maxima, Utrecht","P+R Science Park, Utrecht"};

    private JList jList;
    private int startStationIndex;
    private int endStationIndex;
    private String startStation;
    private String endStation;
    private static double distance;
    private String[] defaultHaltes;
    private ResourceBundle chosenLanguage;
    private String selectedTime = "08:00";
    private static boolean forwardDirection = true;

    BusDataModel dataModel;

     // first constructor is called if the use travels from  current Location

    public BusStops(String userLocation, String userName, boolean fromCurrentLocation ,String userResidence,String userNearestBusStop, String lang, String country) {
        switch (userResidence) {
            case "Amersfoort":
                defaultHaltes = haltesAmersfoort;
                break;
            case "Utrecht":
                defaultHaltes = haltesUtrecht;
                break;
            case "Deventer":
                defaultHaltes = haltesDeventer;
                break;
        }
        dataModel = new BusDataModel();
        getStartPanel(userLocation, userName, fromCurrentLocation, defaultHaltes,userNearestBusStop, lang, country);
    }

    // second constructor is called if the current location not defined

    public BusStops( String userLocation, String userName,boolean fromCurrentLocation,String UserResidence, String lang, String country) {
        switch (UserResidence) {
            case "Amersfoort":
                defaultHaltes = haltesAmersfoort;
                break;
            case "Utrecht":
                defaultHaltes = haltesUtrecht;
                break;
            case "Deventer":
                defaultHaltes = haltesDeventer;
                break;
        }
        getStartPanel(userLocation, userName, fromCurrentLocation, defaultHaltes,null,lang, country);
    }

    private JPanel getStartPanel (String userLocation, String userName, boolean fromCurrentLocation, String[] defaultValues,String userNearestBusStation, String lang, String country) {
        //initialization of static variables lang & country
        Locale.setDefault(new Locale(lang, country));
        chosenLanguage = ResourceBundle.getBundle("bundle");
        // init of start JPanel with start info
        JPanel startPanel=new JPanel();
        String[] timesString = { "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00",
                "11:30", "12:00","12:30","13:00","13:30", "14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30",
                "19:00","19:30","20:00","20:30","21:00","21:30","22:00","22:30","00:00"};
        jList = createJList(defaultValues);

        JList destination = createJList(defaultValues);
        String startString = chosenLanguage.getString("van_bushalte");

        JLabel userNearestBusStationLabel = new JLabel(userNearestBusStation);
        JLabel greetingLabel = new JLabel("Hallo, " + userName);
        JLabel yourNearestStation = new JLabel(chosenLanguage.getString("uw_dichtstbijzijnde_bushalte_is"));
        JLabel yourLocation = new JLabel(userLocation); setLink(yourLocation);

        setFont(yourNearestStation); setFont(yourLocation); setFont(greetingLabel);

        JLabel startLabel = new JLabel(startString);
        JTextField startTextField = createTextField(jList);

        // start text field is visible if boolean fromCurentLocation is false, otherwise start text field is invisible (if user travels from current location, there is no need for start text fied)
        startTextField.setVisible(fromCurrentLocation?false:true);
        String destinationString = fromCurrentLocation? chosenLanguage.getString("kies_uw_bestemming"):chosenLanguage.getString("naar_bushalte");
        JLabel destinationLabel = new JLabel(destinationString); setFont(destinationLabel);
        JTextField destinationTextField = createTextField(destination);

        JPanel leftPanel = new JPanel();
        add(leftPanel);

        JPanel rightPanel = new JPanel();
        add(rightPanel);

        rightPanel.setBorder(new LineBorder(Color.LIGHT_GRAY));
        leftPanel.setPreferredSize(new Dimension(205, 455));

        leftPanel.setBorder(new LineBorder(Color.lightGray));
        leftPanel.setLayout(new GridLayout(0, 1));
        rightPanel.setPreferredSize(new Dimension(424,455));

        JPanel textPanel = new JPanel();
        JPanel destinationTextPanel = new JPanel();

        Color lightYellow = new Color(247, 244, 158);
        textPanel.setBackground(lightYellow );
        destinationTextPanel.setBackground(lightYellow);
        leftPanel.add(fromCurrentLocation?greetingLabel:textPanel);

        leftPanel.add(fromCurrentLocation?yourNearestStation:startTextField);
        JComboBox<String> timeSelect = new JComboBox<>(timesString);

        textPanel.add(startTextField);textPanel.add(startLabel);textPanel.add(timeSelect);
        JScrollPane departScroll = new JScrollPane(jList);

        leftPanel.add(fromCurrentLocation?userNearestBusStationLabel:departScroll);
        leftPanel.add(destinationLabel); leftPanel.add(destinationTextField);

        JScrollPane destinatioScroll = new JScrollPane(destination);
        destinationTextPanel.add(destinationTextField); destinationTextPanel.add(destinationLabel);

        leftPanel.add(destinationTextPanel);
        leftPanel.add(destinatioScroll);

        ImageIcon reverse = new ImageIcon(Functions.getUrl("reverse.png"));
        JLabel reverseLabel = new JLabel(reverse);

        // reverse label is only to seen, if fromCurrentLocation is false
        reverseLabel.setVisible(fromCurrentLocation?false:true);
        leftPanel.add(reverseLabel);
        reverseLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        reverseLabel.setForeground(Color.BLUE.darker());
        timeSelect.addItemListener((ItemEvent e) -> {
            Object item = e.getItem();
            if (e.getStateChange() == ItemEvent.SELECTED) {
                // Item has been selected
                selectedTime = (String) item;
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                // Item has been deselected
                selectedTime = null;

            }
        });


        leftPanel.add (new JButton(new AbstractAction(chosenLanguage.getString("plan_uw_rijs")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateTextField(startTextField,destinationTextField,fromCurrentLocation);
                try {
                    //get startStation from textField if user travels not from current location, otherwise startStation = user Nearest BusStation
                    startStation = fromCurrentLocation? userNearestBusStation:startTextField.getText();
                    endStation = destinationTextField.getText();
                } catch(NullPointerException nullEr) {
                    //System.out.print("station not found");
                }

                TreeMap<LocalTime, NavigableMap<Integer,String>> allDirection = getAllDirections(endStation, selectedTime);
                for (Map.Entry<LocalTime, NavigableMap<Integer,String>> entry : allDirection.entrySet()) {
                    LocalTime key = entry.getKey();
                    NavigableMap<Integer,String> value= entry.getValue();
                    JTree tree = createTree(startStation, endStation, value,key);
                    rightPanel.add(tree);
                    rightPanel.validate();
                    rightPanel.repaint();
                }
            }
        }));

        // mouse listener
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 1) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        startTextField.setText(o.toString());

                    }
                }
            }
        };

        //add mouse listener
        jList.addMouseListener(mouseListener);

        MouseListener destinantionListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 1) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        destinationTextField.setText(o.toString());

                    }
                }
            }
        };

        //add mouse listener
        destination.addMouseListener(destinantionListener);

        //mouse listener reverse label
        reverseLabel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                String startText = startTextField.getText();
                String destinationText = destinationTextField.getText();
                startTextField.setText(destinationText);
                destinationTextField.setText(startText);
            }
        });
        return startPanel;
    }

    private JTree createTree (String startStation, String endStation, NavigableMap<Integer,String> map,LocalTime userTime) {
        startStationIndex = getKey(map,startStation);
        endStationIndex = getKey(map,endStation);
        String direction = getDirection(startStationIndex,endStationIndex,map);
        String vertrektijd = String.valueOf(userTime);
        String busLine = getBusLine(startStation);
        ArrayList<String> tussenhaltes = getIntermediateStops(startStationIndex,endStationIndex,map);

        Double distance = getDistance(startStationIndex,endStationIndex,map);
        BusTijd busTijdObject = new BusTijd(busLine,startStation, endStation,direction,distance,tussenhaltes);
        busTijdObject.setDepartureTime(vertrektijd);
        String aankomstTijd = String.valueOf(calculateTime(distance,vertrektijd));
        busTijdObject.setArrivalTime(aankomstTijd);

        DefaultMutableTreeNode parentNode = new DefaultMutableTreeNode(chosenLanguage.getString("uw_reis")+startStation+"-"+endStation);
        DefaultMutableTreeNode direrctionNode = new DefaultMutableTreeNode(chosenLanguage.getString("bus_richting")+busTijdObject.getDirection());
        DefaultMutableTreeNode lineNode = new DefaultMutableTreeNode(chosenLanguage.getString("bus_lijn")+busTijdObject.getLine());
        DefaultMutableTreeNode distanceNode = new DefaultMutableTreeNode(chosenLanguage.getString("afstand_km") +busTijdObject.getDistance() + " km");
        DefaultMutableTreeNode timeNode = new DefaultMutableTreeNode(chosenLanguage.getString("vertrektijden")+busTijdObject.getDepartureTime());
        DefaultMutableTreeNode arrivalTimeNode = new DefaultMutableTreeNode(chosenLanguage.getString("aankomsttijd")+busTijdObject.getArrivalTime());
        DefaultMutableTreeNode stationsNodes = new DefaultMutableTreeNode(chosenLanguage.getString("Toon_tussenstops"));

         //put intermediate stops in as tree nodes
        tussenhaltes.stream().forEach(station->stationsNodes.add(new DefaultMutableTreeNode(station)));
        parentNode.add(direrctionNode);
        parentNode.add(lineNode);
        parentNode.add(distanceNode);
        parentNode.add(timeNode);
        parentNode.add(arrivalTimeNode);
        parentNode.add(stationsNodes);

        Icon icon = new ImageIcon(Functions.getUrl("info.png"));
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
        renderer.setOpenIcon(icon);
        renderer.setClosedIcon(icon);
        renderer.setLeafIcon(icon);
        final JTree tree = new JTree(parentNode);
        tree.setCellRenderer(renderer);
        return tree;
    }

    private JTextField createTextField(JList list) {
        final JTextField field = new JTextField(15);
        field.getDocument().addDocumentListener(new DocumentListener(){
            @Override public void insertUpdate(DocumentEvent e) { filter(); }
            @Override public void removeUpdate(DocumentEvent e) { filter(); }
            @Override public void changedUpdate(DocumentEvent e) {}
            private void filter() {
                String filter = field.getText();
                filterModel((DefaultListModel<String>)list.getModel(), filter, haltesAmersfoort);
            }
        });
        return field;
    }

    private JList createJList(String[] defaultValues) {
        JList list = new JList(createDefaultListModel(defaultValues));
        list.setVisibleRowCount(6);
        return list;
    }

    private ListModel<String> createDefaultListModel(String[] defaultValues) {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (String s : defaultValues) {
            model.addElement(s);
        }
        return model;
    }

    public void filterModel(DefaultListModel<String> model, String filter, String[] defaultValues) {
        for (String s : defaultValues) {
            if (!s.startsWith(filter)) {
                if (model.contains(s)) {
                    model.removeElement(s);
                }
            } else {
                if (!model.contains(s)) {
                    model.addElement(s);
                }
            }
        }
    }

// getDistance function is more exactly elaborated in a train model. For the bus model, the standart distance between 2 stops is captured as ca. 0,2 km
    public static double getDistance (int startStationIndex, int endStationIndex, NavigableMap<Integer, String> map) {
        //calculate distance for ascending direction
        if (endStationIndex>startStationIndex) {
            NavigableMap<Integer, String> submap = map.subMap(startStationIndex, false, endStationIndex, true);
            distance = 0;
            IntStream.range(0, submap.size())
                    .forEach(index-> distance += 0.2);
            return distance;
        } else {
            NavigableMap<Integer, String> submap = map.descendingMap().subMap(startStationIndex, true, endStationIndex, false);
            distance = 0;
            IntStream.range(0, submap.size())
                    .forEach(index-> distance += 0.2);
            return distance;
        }
    }

    public static ArrayList<String> getIntermediateStops(int startStationIndex, int endStationIndex, NavigableMap<Integer, String> map) {
        if (forwardDirection) {
            NavigableMap<Integer, String> submap = map.subMap(startStationIndex, true, endStationIndex, true);
            ArrayList<String> intermediateStops = submap.entrySet().stream()
                    .map(x->x.getValue())
                    .collect(Collectors
                            .toCollection(ArrayList::new));
            return intermediateStops;
        }
        // for backward direction inverse map
        else {
            NavigableMap<Integer, String> submap = map.descendingMap().subMap(startStationIndex, true, endStationIndex, true);
            ArrayList<String> intermediateStops = submap.entrySet().stream()
                    .map(x->x.getValue())
                    .collect(Collectors
                            .toCollection(ArrayList::new));
            return intermediateStops;
        }
    }

    public LocalTime calculateTime(double distance, String userStartTime) {
        // het reistijd wordt berekent met gemiddelde snelheid 0,01 km/sec
        double speed = 0.01;
        String[] splitted=Functions.getUserTime(userStartTime);
        int hour= Integer.parseInt(splitted[0]);
        int min= Integer.parseInt(splitted[1]);
        long reistijdInSeconds = Math.round(distance / speed);
        LocalTime startTime = LocalTime.of(hour,min);
        LocalTime travelTime = startTime.plusSeconds(reistijdInSeconds);
        return travelTime;
    }

    public  String getMapValue  (NavigableMap<Integer, String> map, String value) {
        String resultSet = map.entrySet().stream()
                .map(x->x.getValue())
                .filter(v->value.equals(v)).findAny().orElse(null);
        return resultSet;
    }

    public static int getKey (NavigableMap<Integer, String> map, String Value) {
        Optional<Integer> key = map.entrySet().stream()
                .filter(v -> Value.equals(v.getValue()))
                .map(Map.Entry::getKey)
                .findFirst();
        return key.isPresent() ? key.get() : 0;
    }


    public static String getDirection(int startStationIndex,int endStationIndex, NavigableMap <Integer, String> map) {
        // define direction by compare last en first stop indexes
        forwardDirection = (endStationIndex>startStationIndex)?true:false;
        List<String> values = map.entrySet().stream()
                .map(x->x.getValue())
                .collect(Collectors.toList());

        // retireve the start end end stops from data
        String  directionDescending = values.get(0);

        //get last city:
        int size = values.size()-1;
        String directionAscending = values.get(size);
        String direction = forwardDirection?directionAscending:directionDescending;
        return direction;
    }

    public TreeMap<LocalTime, NavigableMap<Integer,String>> getAllDirections(String endStop, String selectedTime) {
        TreeMap<LocalTime, NavigableMap<Integer,String>> timeMap = dataModel.getMapofTimes(endStop,selectedTime);
        return timeMap;
    }

    public String getBusLine(String startBusStop) {
        String busLine = dataModel.getLine(startBusStop);
        return busLine;
    }

    public void setFont(JLabel label) {
        label.setFont(new Font("verdana", Font.PLAIN, 12));
    }

    public void setLink(JLabel link) {
        link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        link.setForeground(Color.BLUE.darker());
    }

    public boolean validateTextField(JTextField startTextField, JTextField destinationTextField, boolean fromCurrentLocation) {
        boolean validation;
        // check if the text field not empty
        if (fromCurrentLocation && destinationTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, chosenLanguage.getString("empty_imput"));
            validation = false;
            return validation;
        } else if (fromCurrentLocation == false && (startTextField.getText().isEmpty() || destinationTextField.getText().isEmpty())) {
            JOptionPane.showMessageDialog(null, chosenLanguage.getString("empty_imput"));
            validation = false;
            return validation;
        } else if (!startTextField.getText().isEmpty() && !destinationTextField.getText().isEmpty()) {
            validation = true;
            return validation;
        }
        return false;
    }
}