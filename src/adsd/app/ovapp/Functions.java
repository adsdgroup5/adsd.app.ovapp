package adsd.app.ovapp;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class Functions {

    private static int BUFFER_SIZE = 128000;
    private static File soundFile;
    private static AudioInputStream audioStream;
    private static AudioFormat audioFormat;
    private static SourceDataLine sourceLine;


    public static URL getUrl(String file) {
        java.net.URL imgURL= Functions.class.getResource("/resources/images/"+file);
        return imgURL;

    }

    public static URL  getFileUrl (String file) {
        URL url = Functions.class.getResource("/resources/"+file);
        //System.out.println(path);
        return  url;
    }

    public static String  getPathToJson (String file) {
        String path = Functions.class.getResource("/resources/json/"+file).getFile();
        // System.out.println(path);
        return  path;
    }



    public static String[] getUserTime(String userTime) {
        String[] splitted=userTime.split(":");
        return splitted;

    }


    public static void setLink(JLabel link) {
        link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        link.setForeground(Color.BLUE.darker());
        link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
}

